# Copyright (c) 2022 Huawei Device Co., Ltd.
#
# HDF is dual licensed: you can use it either under the terms of
# the GPL, or the BSD license, at your option.
# See the LICENSE file in the root of this repository for complete details.
# This is an automatically generated file, the copyright header is for illustration only

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")

ohos_shared_library("lib${peripheral_name}_service_1.0") {
  include_dirs = []
  sources = [ "${peripheral_name}_service.cpp" ]
  public_deps = [ "//drivers/interface/${peripheral_name}/v1_0:${peripheral_name}_idl_headers" ]

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_single",
      "utils_base:utils",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_${peripheral_name}"
}

ohos_shared_library("lib${peripheral_name}_driver") {
  include_dirs = []
  sources = [ "${peripheral_name}_driver.cpp" ]
  deps = [ "//drivers/interface/${peripheral_name}/v1_0:lib${peripheral_name}_stub_1.0" ]

  if (is_standard_system) {
    external_deps = [
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_single",
      "utils_base:utils",
    ]
  } else {
    external_deps = [
      "hilog:libhilog",
      "ipc:ipc_single",
    ]
  }

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_${peripheral_name}"
}

group("hdf_${peripheral_name}_service") {
  deps = [
    ":lib${peripheral_name}_driver",
    ":lib${peripheral_name}_service_1.0",
  ]
}